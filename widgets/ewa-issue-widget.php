<?php
/**
 * EWA Elementor Issue Widget.
 *
 * Elementor widget that inserts issue into the page
 *
 * @since 1.0.0
 */
class EWA_Issue_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve issue widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-issue-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve issue widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Issue', 'ewa-elementor-awareness' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve issue widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-bars';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the slider widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-elements' ];
	}

	/**
	 * Register issue widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

        // start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-awareness'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// Issue Heading 
		$this->add_control(
		    'ewa_issue_heading',
			[
			    'label' => esc_html__('Heading','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Heading','ewa-elementor-awareness'),
			]
		);
		
		// Issue Icon Code 
		$this->add_control(
		    'ewa_issue_icon_code',
			[
			    'label' => esc_html__('Icon','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Icon Code','ewa-elementor-awareness'),
			]
		);
		
		// Issue Value 
		$this->add_control(
		    'ewa_issue_value',
			[
			    'label' => esc_html__('Value','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Value','ewa-elementor-awareness'),
			]
		);
		
		// Issue Unit 
		$this->add_control(
		    'ewa_issue_unit',
			[
			    'label' => esc_html__('Unit','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Unit','ewa-elementor-awareness'),
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section

		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-awareness' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-awareness' ),
			]
		);

		// Issue Heading Options
		$this->add_control(
			'ewa_issue_heading_options',
			[
				'label' => esc_html__( 'Heading', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Issue Heading Color
		$this->add_control(
			'ewa_issue_heading_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .global-temperature__text h4' => 'color: {{VALUE}}',
				],
			]
		);

		// Issue Heading Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_issue_heading_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .global-temperature__text h4',
			]
		);

		// Issue Icon Options
		$this->add_control(
			'ewa_issue_icon_options',
			[
				'label' => esc_html__( 'Icon', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Issue Icon Color
		$this->add_control(
			'ewa_issue_icon_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .global-temperature__text i' => 'color: {{VALUE}}',
				],
			]
		);

		// Issue Value Options
		$this->add_control(
			'ewa_issue_value_options',
			[
				'label' => esc_html__( 'Value', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Issue Value Color
		$this->add_control(
			'ewa_issue_value_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .global-temperature__text .value' => 'color: {{VALUE}}',
				],
			]
		);

		// Issue Value Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_issue_value_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .global-temperature__text .value',
			]
		);

		// Issue Unit Options
		$this->add_control(
			'ewa_issue_unit_options',
			[
				'label' => esc_html__( 'Unit', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Issue Unit Color
		$this->add_control(
			'ewa_issue_unit_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .global-temperature__text .value__unit' => 'color: {{VALUE}}',
				],
			]
		);

		// Issue Unit Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_issue_unit_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .global-temperature__text .value__unit',
			]
		);

		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-awareness' ),
			]
		);	

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section
	}

	/**
	 * Render issue widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */

	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$issue_heading = $settings['ewa_issue_heading'];
		$issue_icon = $settings['ewa_issue_icon_code'];
		$issue_value = $settings['ewa_issue_value'];
		$issue_unit = $settings['ewa_issue_unit'];

       ?>

       	<!-- Issue Start Here -->
       		
			<div class="global-temperature__text">
				<h4><?php echo $issue_heading; ?></h4>
			    <p><?php echo $issue_icon; ?>
					<span class="value"><?php echo $issue_value; ?></span>
					<sup class="value__unit"><?php echo $issue_unit; ?></sup>
				</p>
			</div> <!-- end of global-temperature__text -->
		<!-- Issue End Here -->

       <?php
	}
}