<?php
/**
 * EWA Elementor Contact Details Widget.
 *
 * Elementor widget that inserts a contact into the page
 *
 * @since 1.0.0
 */
class EWA_Contact_Details_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve contact details widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-contact-details-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve contact details widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Contact Details', 'ewa-elementor-extension' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve contact details widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-envelope';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the contact details widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-elements' ];
	}

	/**
	 * Register contact details widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		// start of Controls Section
		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'ewa-elementor-extension' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		// Contact Info Title 
		$this->add_control(
			'ewa_contact_info_title',
			[
				'label' => esc_html__( 'Contact Info Title', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => 'Contact us',
				'placeholder' => esc_html__( 'Contact Info Title', 'ewa-elementor-extension' ),
			]
		);

		// Contact Info Address
		$this->add_control(
			'ewa_contact_info_adress',
			[
				'label' => esc_html__( 'Contact Info Address', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => 'Address: 3100 C/A Mouchak, Laredo, Texas, USA',
				'placeholder' => esc_html__( 'Contact Info Address', 'ewa-elementor-extension' ),
			]
		);

		// Contact Info Phone
		$this->add_control(
			'ewa_contact_info_phone',
			[
				'label' => esc_html__( 'Contact Info Phone', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => 'Phone: +1 999-333-4444',
				'placeholder' => esc_html__( 'Contact Info Phone', 'ewa-elementor-extension' ),
			]
		);

		// Contact Info Email
		$this->add_control(
			'ewa_contact_info_email',
			[
				'label' => esc_html__( 'Contact Info Email', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => 'Email: +1 999-333-4444',
				'placeholder' => esc_html__( 'Contact Info Email', 'ewa-elementor-extension' ),
			]
		);

		// Contact Info Email Link
		$this->add_control(
        	'ewa_contact_info_email_link',
			[
				'label'         => esc_html__('Contact Info Email Link', 'ewa-elementor-extension'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
        );

		// Contact Info Website
		$this->add_control(
			'ewa_contact_info_website',
			[
				'label' => esc_html__( 'Contact Info Website', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => 'Website: http://www.yourdomain.com',
				'placeholder' => esc_html__( 'Contact Info Website', 'ewa-elementor-extension' ),
			]
		);

		// Contact Info Website Link
		$this->add_control(
        	'ewa_contact_info_website_link',
			[
				'label'         => esc_html__('Contact Info Website Link', 'ewa-elementor-extension'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
		);
		
		// Contact Info Social Title
		$this->add_control(
			'ewa_contact_social_title',
			[
				'label' => esc_html__( 'Social Info Title', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => 'We Are Social',
				'placeholder' => esc_html__( 'Social Info Title', 'ewa-elementor-extension' ),
			]
		);

		// Contact Info Social Facebook Link
		$this->add_control(
        	'ewa_contact_social_facebook_link',
			[
				'label'         => esc_html__('Facebook Link', 'ewa-elementor-extension'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
		);
		
		// Contact Info Social Twitter Link
		$this->add_control(
        	'ewa_contact_social_twitter_link',
			[
				'label'         => esc_html__('Twitter Link', 'ewa-elementor-extension'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
		);
		
		// Contact Info Social Instagram Link
		$this->add_control(
        	'ewa_contact_social_instagram_link',
			[
				'label'         => esc_html__('Instagram Link', 'ewa-elementor-extension'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
		);
		
		// Contact Info Social Linkedin Link
		$this->add_control(
        	'ewa_contact_social_linkedin_link',
			[
				'label'         => esc_html__('Linkedin Link', 'ewa-elementor-extension'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
		);		

		$this->end_controls_section();

		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-extension' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-extension' ),
			]
		);

		// Contact Info Options
		$this->add_control(
			'ewa_contact_info_options',
			[
				'label' => esc_html__( 'Contact Info', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Contact Info Color
		$this->add_control(
			'ewa_contact_info_color',
			[
				'label' => esc_html__( 'Info Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
				'type' => \Elementor\Scheme_Color::get_type(),
				'value' => \Elementor\Scheme_Color::COLOR_1,
			],
				'default' => '#24262a',
				'selectors' => [
				'{{WRAPPER}} .contact-info address, .contact-info address a' => 'color: {{VALUE}}',
				],
			]
		);

		// Contact Info Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_contact_info_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .contact-info address, .contact-info address a',
			]
		);

		// Contact Info Icon Color
		$this->add_control(
			'ewa_contact_info_icon_color',
			[
				'label' => esc_html__( 'Icon Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
				'type' => \Elementor\Scheme_Color::get_type(),
				'value' => \Elementor\Scheme_Color::COLOR_1,
			],
				'default' => '#fff',
				'selectors' => [
				'{{WRAPPER}} .contact-info li i' => 'color: {{VALUE}}',
				],
			]
		);

		// Contact Info Icon Background
		$this->add_control(
			'ewa_contact_info_icon_background',
			[
				'label' => esc_html__( 'Icon Background', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
				'type' => \Elementor\Scheme_Color::get_type(),
				'value' => \Elementor\Scheme_Color::COLOR_1,
			],
				'default' => '#77C720',
				'selectors' => [
				'{{WRAPPER}} .contact-info li i' => 'background-color: {{VALUE}}',
				],
			]
		);


		// Social Info Options
		$this->add_control(
			'ewa_contact_social_options',
			[
				'label' => esc_html__( 'Social Info', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Social Info Color
		$this->add_control(
			'ewa_contact_social_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
				'type' => \Elementor\Scheme_Color::get_type(),
				'value' => \Elementor\Scheme_Color::COLOR_1,
			],
				'default' => '#fff',
				'selectors' => [
				'{{WRAPPER}} .contact-social li a' => 'color: {{VALUE}}',
				],
			]
		);

		// Social Info Background Color
		$this->add_control(
			'ewa_contact_social_background',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
				'type' => \Elementor\Scheme_Color::get_type(),
				'value' => \Elementor\Scheme_Color::COLOR_1,
			],
				'selectors' => [
				'{{WRAPPER}} .contact-social li' => 'background-color: {{VALUE}}',
				],
			]
		);


		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-extension' ),
			]
		);	
		
		// Contact Info Hover Options
		$this->add_control(
			'ewa_contact_info_hover_options',
			[
				'label' => esc_html__( 'Contact Info', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Contact Info Hover Color
		$this->add_control(
			'ewa_contact_info_hover_color',
			[
			  'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
			  'type' => \Elementor\Controls_Manager::COLOR,
			  'scheme' => [
				  'type' => \Elementor\Scheme_Color::get_type(),
				  'value' => \Elementor\Scheme_Color::COLOR_1,
			  ],
			  'default' => '#77C720',
			  'selectors' => [
				  '{{WRAPPER}} .contact-info address a:hover' => 'color: {{VALUE}}',
			  ],
			]
		  );
		  
		  // Social Info Hover Options
		$this->add_control(
			'ewa_social_info_hover_options',
			[
				'label' => esc_html__( 'Social Info', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Social Info Hover Color
		$this->add_control(
			'ewa_contact_social_hover_color',
			[
			  'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
			  'type' => \Elementor\Controls_Manager::COLOR,
			  'scheme' => [
				  'type' => \Elementor\Scheme_Color::get_type(),
				  'value' => \Elementor\Scheme_Color::COLOR_1,
			  ],
			  'default' => '#fff',
			  'selectors' => [
				  '{{WRAPPER}} .contact-social li a:hover' => 'color: {{VALUE}}',
			  ],
			]
		);
		  
		// Social Info Hover Background Color
		$this->add_control(
			'ewa_contact_social_hover_background',
			[
			  'label' => esc_html__( 'Background Color', 'ewa-elementor-extension' ),
			  'type' => \Elementor\Controls_Manager::COLOR,
			  'scheme' => [
				  'type' => \Elementor\Scheme_Color::get_type(),
				  'value' => \Elementor\Scheme_Color::COLOR_1,
			  ],
			  'default' => '#77C720',
			  'selectors' => [
				  '{{WRAPPER}} .contact-social li a:hover' => 'background-color: {{VALUE}}',
			  ],
			]
	  	);

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render contact details widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();

		$contact_info_title = $settings['ewa_contact_info_title'];
		$contact_info_adress = $settings['ewa_contact_info_adress'];
		$contact_info_phone = $settings['ewa_contact_info_phone'];
		$contact_info_email = $settings['ewa_contact_info_email'];
		$contact_info_email_link = $settings['ewa_contact_info_email_link']['url'];
		$contact_info_website = $settings['ewa_contact_info_website'];
		$contact_info_website_link = $settings['ewa_contact_info_website_link']['url'];
		$contact_social_title = $settings['ewa_contact_social_title'];
		$contact_social_facebook_link = $settings['ewa_contact_social_facebook_link']['url'];
		$contact_social_twitter_link = $settings['ewa_contact_social_twitter_link']['url'];
		$contact_social_instagram_link = $settings['ewa_contact_social_instagram_link']['url'];
		$contact_social_linkedin_link = $settings['ewa_contact_social_linkedin_link']['url'];
       ?>
        	<div class="contact-address">
				<h2 class="contact-title"><?php echo $contact_info_title;?></h2>
				<ul class="contact-info">
                    <li>
                        <address><i class="fas fa-home"></i><?php echo $contact_info_adress;?></address>
                    </li>
                    <li>
                        <address><i class="fas fa-phone-alt"></i><?php echo $contact_info_phone;?></address>
                    </li>
                    <li>
                        <address><i class="fas fa-envelope"></i><a href="mailto:<?php echo esc_url($contact_info_email_link);?>"><?php echo $contact_info_email;?></a></address>
                    </li>
                    <li>
                        <address><i class="fas fa-globe-europe"></i><a href="<?php echo esc_url($contact_info_website_link);?>" target="_blank"><?php echo $contact_info_website;?></a></address>
                    </li>
                </ul>
			</div>
			<h2 class="contact-title"><?php echo $contact_social_title;?></h2>
				<ul class="contact-social">
                    <li>
                        <a href="<?php echo esc_url($contact_social_facebook_link);?>"><i class="fab fa-twitter"></i></a>
                    </li>
                    <li>
					<a href="<?php echo esc_url($contact_social_twitter_link);?>"><i class="fab fa-facebook-f"></i></a>
                    </li>
                    <li>
					<a href="<?php echo esc_url($contact_social_instagram_link);?>"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
					<a href="<?php echo esc_url($contact_social_linkedin_link);?>"><i class="fab fa-pinterest-p"></i></a>
                    </li>
                </ul>
			</div>
       <?php
	}
}