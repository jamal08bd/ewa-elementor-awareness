<?php
/**
 * EWA Elementor Price Plan Widget.
 *
 * Elementor widget that inserts price plan into the page
 *
 * @since 1.0.0
 */
class EWA_Price_Plan_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve price plan widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-price-plan-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve price plan widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Price Plan', 'ewa-elementor-awareness' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve price plan widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-money-bill-alt';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the price plan widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-elements' ];
	}

	/**
	 * Register price plan widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

	}

	/**
	 * Render price plan widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();

       ?>

       	<!-- Price plan Start Here -->
			<h3>HTML for Price plan goes here</h3>
		<!-- Price plan End Here -->

       <?php
	}
}