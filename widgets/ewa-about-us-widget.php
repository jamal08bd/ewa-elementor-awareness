<?php
/**
 * EWA Elementor About Us Widget.
 *
 * Elementor widget that inserts about us into the page
 *
 * @since 1.0.0
 */
class EWA_About_Us_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve about us widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-about-us-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve about us widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA About Us', 'ewa-elementor-awareness' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve about us widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-address-card';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the about us widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-elements' ];
	}

	/**
	 * Register about us widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
        
		// start of the Content tab section
	   	$this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-awareness'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// About Us Heading 
		$this->add_control(
		    'ewa_about_us_heading',
			[
			    'label' => esc_html__('Heading','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__('Stop polluters','ewa-elementor-awareness'),
			]
		);
		
		//About Us Description
		$this->add_control(
		    'ewa_about_us_des',
			[
			    'label' => esc_html__('Description','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'default' => esc_html__('Polluters must quit debilitating the privileges of defense less groups and undermining atmosphere science and activity. Give an application directly to the local mayor and we will be forwarding it.','ewa-elementor-awareness'),
			]
		);

		// About Us Button Text 
		$this->add_control(
		    'ewa_about_us_text',
			[
			    'label' => esc_html__('Button Text','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__('take action','ewa-elementor-awareness'),
			]
		);
		
		//About Us Button Link
		$this->add_control(
		    'ewa_about_us_link',
			[
			    'label'         => esc_html__('Button Link','ewa-elementor-awareness'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);
		
		//About Us Image
		$this->add_control(
		    'ewa_about_us_image',
			[
			    'label' => esc_html__('Choose Image','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
				    'url' => \Elementor\Utils::get_placeholder_image_src(),          
				],
			]
		);
		
		//About Us Icon Link
		$this->add_control(
		    'ewa_about_us_icon_link',
			[
			    'label'         => esc_html__('Icon Link','ewa-elementor-awareness'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section

		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-awareness' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-awareness' ),
			]
		);

		// About Heading Options
		$this->add_control(
			'ewa_about_heading_options',
			[
				'label' => esc_html__( 'Heading', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// About Heading Color
		$this->add_control(
			'ewa_about_heading_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .about-action__heading' => 'color: {{VALUE}}',
				],
			]
		);

		// About Heading Separator Color
		$this->add_control(
			'ewa_about_heading_separator_color',
			[
				'label' => esc_html__( 'Separator Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .about-action__heading:before' => 'color: {{VALUE}}',
				],
			]
		);

		// About Heading Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_about_heading_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .about-action__heading',
			]
		);

		// About Heading Separator Color
		$this->add_control(
			'ewa_about_heading_separator_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .about-action__heading:before' => 'color: {{VALUE}}',
				],
			]
		);

		// About Description Options
		$this->add_control(
			'ewa_about_des_options',
			[
				'label' => esc_html__( 'Description', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// About Description Color
		$this->add_control(
			'ewa_about_des_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .about-action__text p' => 'color: {{VALUE}}',
				],
			]
		);

		// About Description Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_about_des_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .about-action__text p',
			]
		);

		// About Button Options
		$this->add_control(
			'ewa_about_btn_options',
			[
				'label' => esc_html__( 'Button', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// About Button Color
		$this->add_control(
			'ewa_about_btn_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .about-action__button' => 'color: {{VALUE}}',
				],
			]
		);

		// About Button Border Color
		$this->add_control(
			'ewa_about_btn_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .about-action__button' => 'border-color: {{VALUE}}',
				],
			]
		);

		// About Button Background
		$this->add_control(
			'ewa_about_btn_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .about-action__button' => 'background-color: {{VALUE}}',
				],
			]
		);

		// About Button Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_about_btn_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .about-action__button',
			]
		);

		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-awareness' ),
			]
		);
		
		// About Button Options
		$this->add_control(
			'ewa_about_btn_hover_options',
			[
				'label' => esc_html__( 'Button', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// About Button Hover Color
		$this->add_control(
			'ewa_about_btn_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .about-action__button:hover' => 'color: {{VALUE}}',
				],
			]
		);

		// About Button Hover Color
		$this->add_control(
			'ewa_about_btn_hover_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .about-action__button:hover' => 'border-color: {{VALUE}}',
				],
			]
		);

		// About Button Hover Background
		$this->add_control(
			'ewa_about_btn_hover_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .about-action__button:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		// About Button Hover Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_about_btn_hover_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .about-action__button:hover',
			]
		);		

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section
	}

	/**
	 * Render about us widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$about_heading = $settings['ewa_about_us_heading'] ;
		$about_description = $settings['ewa_about_us_des'] ;
		$about_button = $settings['ewa_about_us_text'] ;
		$about_link = $settings['ewa_about_us_link'] ;
		$about_image = $settings['ewa_about_us_image']['url'] ;

       ?>

       	<!-- About us Start Here -->
            <div class="grid">
				<div class="about-action">
				    <div class="about-action__body">
				        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 ">
					        <div class="about-action__text">
                                <h2 class="about-action__heading"><?php echo $about_heading; ?></h2>
                                <p><?php echo $about_description ?></p>
                                <a class="btn btn-outline about-action__button" href="<?php echo esc_url($about_link); ?>"><?php echo $about_button;?></a>							  
                            </div> <!--  end .Action-text  -->
					    </div> <!-- end of .col-xs -->
				        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
					        <figure class="about-action__image">
						          <img src="<?php echo $about_image; ?>" />
				            </figure> <!-- end of about-action-image -->
                            <div class="about-action__bg"></div>						
	  				    </div> <!-- end of .col-xs -->
			        </div> <!-- end of about-action__body -->
				</div> <!-- end of about-action -->
            </div> <!-- end of .grid -->
		<!-- About us End Here -->

       <?php
	}
}