<?php
/**
 * EWA Elementor Post Grid 2 Widget.
 *
 * Elementor widget that inserts a porst grid into the page
 *
 * @since 1.0.0
 */
class EWA_Post_Grid_2_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve post grid widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-post-grid-2-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve post grid widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Post Grid 2', 'ewa-elementor-awareness' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve post grid widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-th';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the post grid widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-elements' ];
	}

	/**
	 * Register post grid widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		// start of Controls Section
		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'ewa-elementor-extension' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		// Source of the posts
        $this->add_control(
        	'ewa_posts_from_categories_by_ids',
			[
				'label' => esc_html__( 'Post from Categories (Enter Category ids separated by comma)', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( '4,5', 'ewa-elementor-extension' ),
			]
        );

		// Post Read More
        $this->add_control(
        	'ewa_posts_readmore_text',
			[
				'label' => esc_html__( 'Button Text', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Read More', 'ewa-elementor-extension' ),
			]
        );

		$this->end_controls_section();
		// end of the source of the posts


		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-extension' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-extension' ),
			]
		);

		// Post Two Date Options
		$this->add_control(
			'ewa_post_two_date_options',
			[
				'label' => esc_html__( 'Date', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Post Two Date Color
		$this->add_control(
			'ewa_post_two_date_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .postgrid-two__date' => 'color: {{VALUE}}',
				],
			]
		);

		// Post Two Date Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_post_two_date_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .postgrid-two__date',
			]
		);

		// Post Two Title Options
		$this->add_control(
			'ewa_post_two_title_options',
			[
				'label' => esc_html__( 'Title', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Post Two Title Color
		$this->add_control(
			'ewa_post_two_title_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .postgrid-two__title a' => 'color: {{VALUE}}',
				],
			]
		);

		// Post Two Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_post_two_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .postgrid-two__title a',
			]
		);

		// Post Two Description Options
		$this->add_control(
			'ewa_post_two_des_options',
			[
				'label' => esc_html__( 'Description', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Post Two Description Color
		$this->add_control(
			'ewa_post_one_link_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .postgrid-two__details p' => 'color: {{VALUE}}',
				],
			]
		);

		// Post Two Description Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_post_two_des_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .postgrid-two__details p',
			]
		);

		// Post Two Link Options
		$this->add_control(
			'ewa_post_two_link_options',
			[
				'label' => esc_html__( 'Button', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Post Two Link Color
		$this->add_control(
			'ewa_post_two_link_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .postgrid-two__link' => 'color: {{VALUE}}',
				],
			]
		);

		// Post Two Link Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_post_two_link_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .postgrid-two__link',
			]
		);

		//  Post Two Content Options
		$this->add_control(
			'ewa_post_two_content_options',
			[
				'label' => esc_html__( 'Content Options', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Post Two Alignment
		$this->add_responsive_control(
			'ewa_post_two_alignment',
			[
				'label' => esc_html__( 'Alignment', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'ewa-elementor-extension' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'ewa-elementor-extension' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'ewa-elementor-extension' ),
						'icon' => 'fa fa-align-right',
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'default' => 'left',
				'selectors' => [
					'{{WRAPPER}} .postgrid-two__body' => 'text-align: {{VALUE}}',
				],
			]
		);

		// Post Two Border Color
		$this->add_control(
			'ewa_post_two_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#dfdfdf',
				'selectors' => [
					'{{WRAPPER}} .postgrid-two__body' => 'border-color: {{VALUE}}',
				],
			]
		);


		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-extension' ),
			]
		);	
		
		// Post Two Title Hover Options
		$this->add_control(
			'ewa_post_two_title_hover_options',
			[
				'label' => esc_html__( 'Title', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Post Two Title Hover Color
		$this->add_control(
			'ewa_post_two_title_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .postgrid-two__title a:hover' => 'color: {{VALUE}}',
				],
			]
		);

		// Post Two Title Hover Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_post_two_title_hover_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .postgrid-two__title a:hover',
			]
		);	
		
		// Post Two Link Hover Options
		$this->add_control(
			'ewa_post_two_link_hover_options',
			[
				'label' => esc_html__( 'Button', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Post Two Link Hover Color
		$this->add_control(
			'ewa_post_two_link_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#6BA031',
				'selectors' => [
					'{{WRAPPER}} .postgrid-two__link:hover' => 'color: {{VALUE}}',
				],
			]
		);

		// Post Two Link Hover Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_post_two_link_hover_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .postgrid-two__link:hover',
			]
		);	

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section
	}

	/**
	 * Render post grid widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();

		$post_grid_title = $settings['ewa_post_grid_title'];

		$posts_readmore_text = $settings['ewa_posts_readmore_text'];

		// post from categories getting category ID's
		$cat_ids = $settings['ewa_posts_from_categories_by_ids'];

        $args = array(
          'posts_per_page' => 3,
          'post_type' => 'post',
          'post_status' => 'publish',
          'ignore_sticky_posts' => 1,
          'cat' => $cat_ids,
        );
        $query = new WP_Query($args);

       ?>

       	<!-- Post grid 2 Start Here -->
			<div class="grid ">
			<div class="postgrid-two">
			<?php 
			if ($query->have_posts()) :

	          while ($query->have_posts()) : $query->the_post(); 

	          	// get the featured image url
				$image_url = get_the_post_thumbnail_url( get_the_ID(), 'large' );
				// get the post date
				$post_date = get_the_date('M j, Y');


				// get the category name and link
			  	$term_list = wp_get_post_terms(get_the_ID(), 'category', ['fields' => 'all']);

			  	$cat_name = '';
			 	$cat_link = '';

			    foreach ($term_list as $term) {
			      if (get_post_meta(get_the_ID(), '_yoast_wpseo_primary_category', true) == $term->term_id) {
			        $cat_name = $term->name;
			        $cat_link = get_term_link($term->term_id);
			       break;
			      }
			      else {
			        $cat_name = $term->name;
			        $cat_link = get_term_link($term->term_id);
			      }
			    }
	        ?>
                
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="postgrid-two__body">
						<a href="<?php esc_url(the_permalink()); ?>" class="postgrid-two__image" style="background-image:url('<?php echo esc_url($image_url); ?>')"></a>
						
						<div class="postgrid-two__details">
							<a class="postgrid-two__date"><?php echo $post_date;?></a>
							<h4 class="postgrid-two__title"><a href="<?php esc_url(the_permalink()); ?>"><?php the_title(); ?></a></h4>
							<?php the_excerpt();?>
							<a class="postgrid-two__link" href="<?php esc_url(the_permalink()); ?>"><?php echo $posts_readmore_text;?></a>
						</div>
					</div> <!-- end of recent-blog__body -->
				</div> <!-- end of col xs -->
				<?php
	  		  endwhile;

	        endif; 
	        ?>
			</div> <!-- end of postgrid-two -->
		</div> <!-- end of grid -->
		<!-- Post grid 2 End Here -->

       <?php
	}
}