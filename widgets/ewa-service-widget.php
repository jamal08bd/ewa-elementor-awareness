<?php
/**
 * EWA Elementor Services Widget.
 *
 * Elementor widget that inserts services into the page
 *
 * @since 1.0.0
 */
class EWA_Service_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve services widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-service-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve services widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Service', 'ewa-elementor-awareness' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve services widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-bars';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the services widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-elements' ];
	}

	/**
	 * Register services widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

        // start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-awareness'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// Service Image
		$this->add_control(
		    'ewa_service_image',
			[
			    'label' => esc_html__('Choose Service Image','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
				    'url' => \Elementor\Utils::get_placeholder_image_src(),          
				],
			]
		);
		
		//Service Title
		$this->add_control(
		    'ewa_service_title',
			[
			    'label' => esc_html__('Title','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__('Enter Service Title','ewa-elementor-awareness'),
			]
		);
		
		//Service Link
		$this->add_control(
		    'ewa_service_link',
			[
			    'label'         => esc_html__('Service Link','ewa-elementor-awareness'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section

		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-awareness' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-awareness' ),
			]
		);

		// Service Title Options
		$this->add_control(
			'ewa_service_title_options',
			[
				'label' => esc_html__( 'Service Title', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Service Title Color
		$this->add_control(
			'ewa_service_title_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .service-one__text h3' => 'color: {{VALUE}}',
				],
			]
		);

		// Service Title Background Color
		$this->add_control(
			'ewa_service_title_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => 'rgba(0, 0, 0, 0.5)',
				'selectors' => [
					'{{WRAPPER}} .service-one__text h3' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Service Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_service_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .service-one__text h3',
			]
		);

		// Service Link Options
		$this->add_control(
			'ewa_service_link_options',
			[
				'label' => esc_html__( 'Service Link', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Service Link Color
		$this->add_control(
			'ewa_service_link_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .service-one__text a' => 'color: {{VALUE}}',
				],
			]
		);

		// Service Link Background
		$this->add_control(
			'ewa_service_link_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .service-one__text a' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-awareness' ),
			]
		);	
		
		// Service Link Hover Options
		$this->add_control(
			'ewa_service_link_hover_options',
			[
				'label' => esc_html__( 'Service Link Hover', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Service Link Color
		$this->add_control(
			'ewa_service_link_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .service-one__text a:hover' => 'color: {{VALUE}}',
				],
			]
		);

		// Service Link Background
		$this->add_control(
			'ewa_service_link_hover_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#dfdfdf',
				'selectors' => [
					'{{WRAPPER}} .service-one__text a:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section
	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$service_image = $settings['ewa_service_image']['url'];
		$service_title = $settings['ewa_service_title'];
		$service_link = $settings['ewa_service_link']['url'];

       ?>

       	<!-- Service Start Here -->
			<div class="service-one__body ">
				<div class="service-one__image " style="background-image: url('<?php echo $service_image; ?>');">
					<div class="service-one__text">
					    <h3><?php echo $service_title;?></h3>
					    <a class="fa fa-arrow-right" href="<?php echo esc_url($service_link); ?>"></a>
					</div>		    
				</div>
			</div> <!-- end of service-how-we-work__body -->
		<!-- Service End Here -->

       <?php
	}
}