<?php
/**
 * EWA Elementor Statistics Widget.
 *
 * Elementor widget that inserts statistics into the page
 *
 * @since 1.0.0
 */
class EWA_Statistics_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve statistics widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-statistics-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve statistics widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Statistics', 'ewa-elementor-awareness' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve statistics widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-sort-amount-up';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the statistics widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-elements' ];
	}

	/**
	 * Register statistics widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		// start of the Content tab section
		$this->start_controls_section(
			'content-section',
			[
				'label' => esc_html__('Content','ewa-elementor-awareness'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
			
			]
		);
		 
		// Statistics Number
		$this->add_control(
			 'ewa_statistics_number',
			[
				 'label' => esc_html__('Statistics Number','ewa-elementor-awareness'),
				 'type' => \Elementor\Controls_Manager::TEXT,
				 'label_block' => true,
				 'default' => esc_html__('250', 'ewa-elementor-awareness')
			]
		);
		 
		// Statistics Text 1
		$this->add_control(
			'ewa_statistics_text1',
			[
				 'label' => esc_html__('Text 1','ewa-elementor-awareness'),
				 'type' => \Elementor\Controls_Manager::TEXT,
				 'label_block' => true,
				 'default' => esc_html__('Running','ewa-elementor-awareness'),
			 ]
		 );
		 
		 // Statistics Text 2
		 $this->add_control(
			 'ewa_statistics_text2',
			 [
				 'label' => esc_html__('Text 2','ewa-elementor-awareness'),
				 'type' => \Elementor\Controls_Manager::TEXT,
				 'label_block' => true,
				 'default' => esc_html__('Campaigns','ewa-elementor-awareness'),
			 ]
		 );
		 
		 $this->end_controls_section();
		 // end of the Content tab section
 
		 // start of the Style tab section
		 $this->start_controls_section(
			 'style_section',
			 [
				 'label' => esc_html__( 'Content Style', 'ewa-elementor-awareness' ),
				 'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			 ]
		 );
 
		 $this->start_controls_tabs(
			 'style_tabs'
		 );
 
		 // start everything related to Normal state here
		 $this->start_controls_tab(
			 'style_normal_tab',
			 [
				 'label' => esc_html__( 'Normal', 'ewa-elementor-awareness' ),
			 ]
		 );
 
		 // Statistics Number Options
		 $this->add_control(
			 'ewa_statistics_number_options',
			 [
				 'label' => esc_html__( 'Statistics Number', 'ewa-elementor-awareness' ),
				 'type' => \Elementor\Controls_Manager::HEADING,
				 'separator' => 'before',
			 ]
		 );
 
		 // Statistics Number Color
		 $this->add_control(
			 'ewa_statistics_number_color',
			 [
				 'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				 'type' => \Elementor\Controls_Manager::COLOR,
				 'scheme' => [
					 'type' => \Elementor\Scheme_Color::get_type(),
					 'value' => \Elementor\Scheme_Color::COLOR_1,
				 ],
				 'default' => '#fff',
				 'selectors' => [
					 '{{WRAPPER}} .statistics-block__number' => 'color: {{VALUE}}',
				 ],
			 ]
		 );
 
		 // Statistics Number Typography
		 $this->add_group_control(
			 \Elementor\Group_Control_Typography::get_type(),
			 [
				 'name' => 'ewa_statistics_number_typography',
				 'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				 'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				 'selector' => '{{WRAPPER}} .statistics-block__number',
			 ]
		 );
 
		 // Statistics Text 1 Options
		 $this->add_control(
			 'ewa_statistics_text1_options',
			 [
				 'label' => esc_html__( 'Statistics Text 1', 'ewa-elementor-awareness' ),
				 'type' => \Elementor\Controls_Manager::HEADING,
				 'separator' => 'before',
			 ]
		 );
 
		 // Statistics Text 1 Color
		 $this->add_control(
			 'ewa_statistics_text1_color',
			 [
				 'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				 'type' => \Elementor\Controls_Manager::COLOR,
				 'scheme' => [
					 'type' => \Elementor\Scheme_Color::get_type(),
					 'value' => \Elementor\Scheme_Color::COLOR_1,
				 ],
				 'default' => '#fff',
				 'selectors' => [
					 '{{WRAPPER}} .statistics-block__details h4' => 'color: {{VALUE}}',
				 ],
			 ]
		 );

		 // Statistics Text 1 Typography
		 $this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_statistics_text1_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .statistics-block__details h4',
			]
		);

		// Statistics Text 2 Options
		$this->add_control(
			'ewa_statistics_text2_options',
			[
				'label' => esc_html__( 'Statistics Text 2', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Statistics Text 2 Color
		$this->add_control(
			'ewa_statistics_text2_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .statistics-block__details h4 span' => 'color: {{VALUE}}',
				],
			]
		);

		// Statistics Text 2 Typography
		$this->add_group_control(
		   \Elementor\Group_Control_Typography::get_type(),
		   [
			   'name' => 'ewa_statistics_text2_typography',
			   'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
			   'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
			   'selector' => '{{WRAPPER}} .statistics-block__details h4 span',
		   ]
	   );
 
		 $this->end_controls_tab();
		 // end everything related to Normal state here
 
		 // start everything related to Hover state here
		 $this->start_controls_tab(
			 'style_hover_tab',
			 [
				 'label' => esc_html__( 'Hover', 'ewa-elementor-awareness' ),
			 ]
		 );	
 
		 $this->end_controls_tab();
		 // end everything related to Hover state here
 
		 $this->end_controls_tabs();
 
		 $this->end_controls_section();
		 // end of the Style tab section

	}

	/**
	 * Render statistics widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();

		$statistic_number = $settings['ewa_statistics_number'];
		$statistic_text1 = $settings['ewa_statistics_text1'];
		$statistic_text2 = $settings['ewa_statistics_text2'];
       ?>

       	<!-- Statistics Start Here -->
			<div class="statistics-block">
				<div class="statistics-block__details">
					<span class="statistics-block__number counter"><?php echo $statistic_number;?></span>
					<h4><?php echo $statistic_text1;?><span><?php echo $statistic_text2;?></span></h4>
				</div>
			</div> <!-- end of statistics-block -->
		<!-- Statistics End Here -->

       <?php
	}
}