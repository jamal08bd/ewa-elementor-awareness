<?php
/**
 * EWA Elementor About Us 2 Widget.
 *
 * Elementor widget that inserts about us 2 into the page
 *
 * @since 1.0.0
 */
class EWA_About_Us_2_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve about us widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-about-us-2-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve about us widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA About Us 2', 'ewa-elementor-awareness' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve about us widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-address-card';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the about us widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-elements' ];
	}

	/**
	 * Register about us widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

        // start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-awareness'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// About Us Two Heading 
		$this->add_control(
		    'ewa_about_us_two_heading',
			[
			    'label' => esc_html__('Heading','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__('Who Are We','ewa-elementor-awareness'),
			]
		);
		
		//About Us Two Description
		$this->add_control(
		    'ewa_about_us_two_des',
			[
			    'label' => esc_html__('About Us Two Description','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'default' => esc_html__('Deforestation is one of the most common issue which is ruining our ecosystem. So, we need to stop cutting down trees and planting more trees around the world.','ewa-elementor-awareness'),
			]
		);

		// About Us Two Button 
		$this->add_control(
		    'ewa_about_us_two_button',
			[
			    'label' => esc_html__('Button','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__('learn more','ewa-elementor-awareness'),
			]
		);
		
		//About Us Two Button Link
		$this->add_control(
		    'ewa_about_us_two_link',
			[
			    'label'         => esc_html__('Button Link','ewa-elementor-awareness'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);
		
		//About Us Two Image
		$this->add_control(
		    'ewa_about_us_two_image',
			[
			    'label' => esc_html__('Choose About Us Two Image','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
				    'url' => \Elementor\Utils::get_placeholder_image_src(),          
				],
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-awareness' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-awareness' ),
			]
		);

		// About Two Heading Options
		$this->add_control(
			'ewa_about_two_heading_options',
			[
				'label' => esc_html__( 'Heading', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// About Two Heading Color
		$this->add_control(
			'ewa_about_two_heading_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .aboutus-two__heading' => 'color: {{VALUE}}',
				],
			]
		);

		// About Two Heading Separator Color
		$this->add_control(
			'ewa_about_two_heading_separator_color',
			[
				'label' => esc_html__( 'Separator Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .aboutus-two__heading:before' => 'color: {{VALUE}}',
				],
			]
		);

		// About Two Heading Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_about_two_heading_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .aboutus-two__heading',
			]
		);

		// About Two Description Options
		$this->add_control(
			'ewa_about_two_des_options',
			[
				'label' => esc_html__( 'Description', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// About Two Description Color
		$this->add_control(
			'ewa_about_two_des_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .aboutus-two__text p' => 'color: {{VALUE}}',
				],
			]
		);

		// About Two Description Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_about_two_des_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .aboutus-two__text p',
			]
		);

		// About Two Button Options
		$this->add_control(
			'ewa_about_two_btn_options',
			[
				'label' => esc_html__( 'Button', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// About Two Button Color
		$this->add_control(
			'ewa_about_two_btn_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .aboutus-two__button' => 'color: {{VALUE}}',
				],
			]
		);

		// About Two Button Border Color
		$this->add_control(
			'ewa_about_two_btn_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .aboutus-two__button' => 'border-color: {{VALUE}}',
				],
			]
		);

		// About Two Button Background
		$this->add_control(
			'ewa_about_two_btn_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .aboutus-two__button' => 'background-color: {{VALUE}}',
				],
			]
		);

		// About Two Button Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_about_two_btn_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .aboutus-two__button',
			]
		);		

		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-awareness' ),
			]
		);
		
		// About Two Button Options
		$this->add_control(
			'ewa_about_two_btn_options',
			[
				'label' => esc_html__( 'Button', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// About Two Button Hover Color
		$this->add_control(
			'ewa_about_two_btn_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .aboutus-two__button:hover' => 'color: {{VALUE}}',
				],
			]
		);

		// About Two Button Hover Border Color
		$this->add_control(
			'ewa_about_two_btn_hover_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .aboutus-two__button:hover' => 'border-color: {{VALUE}}',
				],
			]
		);

		// About Two Button Hover Background
		$this->add_control(
			'ewa_about_two_btn_hover_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .aboutus-two__button:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		// About Two Button Hover Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_about_two_btn_hover_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .aboutus-two__button:hover',
			]
		);		

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section
	}

	/**
	 * Render about us widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$abouttwo_heading = $settings['ewa_about_us_two_heading'];
		$abouttwo_description_one = $settings['ewa_about_us_two_des'];
		$abouttwo_link = $settings['ewa_about_us_two_link'];
		$abouttwo_image = $settings['ewa_about_us_two_image']['url'];

       ?>

       	<!-- About us 2 Start Here -->
            <div class="grid">
				<div class="aboutus-two__body">
				    <div class="col-md-6 col-sm-12 col-xs-12 ">
					    <div class="aboutus-two__text">
                            <h2 class="aboutus-two__heading"><?php echo $abouttwo_heading; ?></h2>
                            <p><?php echo $abouttwo_description_one; ?></p>
                            <a class="btn btn-outline aboutus-two__button" href="<?php echo esc_url($abouttwo_link); ?>">Learn More</a>							  
                        </div> <!--  end .Action-text  -->
					
				    </div> <!-- end of .col-xs -->
				    
					<div class="col-md-6 col-sm-12 col-xs-12">
					    <figure class="aboutus-two__image">
						    <img src="<?php echo $abouttwo_image; ?>" />
						</figure> <!-- end of about-action-image -->
					</div>
					
				</div> <!-- end of aboutus-two__body -->  
		    </div> <!-- end of .grid -->
		<!-- About us 2 End Here -->

       <?php
	}
}