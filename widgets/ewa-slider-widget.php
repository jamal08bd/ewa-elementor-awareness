<?php
/**
 * EWA Elementor Slider Widget.
 *
 * Elementor widget that inserts slider into the page
 *
 * @since 1.0.0
 */
class EWA_Slider_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Slider widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-slider-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve slider widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Slider', 'ewa-elementor-awareness' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve slider widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-sliders-h';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the slider widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-elements' ];
	}

	/**
	 * Register slider widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

       // start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-awareness'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		$repeater = new \Elementor\Repeater ();
		
		// Repeater For Slider Sub Title
		$repeater->add_control(
		    'ewa_slider_sub_title',
			[
			    'label' => esc_html__( 'Sub Title', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Sub Title' , 'ewa-elementor-awareness' ),
			]
		);
		
		// Repeater For Slider Title
		$repeater->add_control(
		    'ewa_slider_title',
            [
			    'label' => esc_html__('Title','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__('Enter Slider Title','ewa-elementor-awareness'),
			]			
		);
		
		// Repeater For Slider Image
		$repeater->add_control(
		    'ewa_slider_image',
			[
			    'label' => esc_html__('Choose Slider Image','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'label_block' => true,
				'default' => [
				    'url' => \Elementor\Utils::get_placeholder_image_src(),          
				],
			]
		);
		
		// Repeater For Slider Button One Text
		$repeater->add_control(
		    'ewa_slider_button_one_text',
			[
			    'label' => esc_html__( 'Button One Text', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Button One Text' , 'ewa-elementor-awareness' ),
			]
		);
		
		// Repeater For Slider Button One Link
		$repeater->add_control(
		    'ewa_slider_button_one_link',
			[
			    'label'         => esc_html__('Slider Button One Link','ewa-elementor-awareness'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);
		
		// Repeater For Slider Button Two Text
		$repeater->add_control(
		    'ewa_slider_button_two_text',
			[
			    'label' => esc_html__( 'Button Two Text', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Button Two Text' , 'ewa-elementor-awareness' ),
			]
		);
		
		// Repeater For Slider Button Two Link
		$repeater->add_control(
		    'ewa_slider_button_two_link',
			[
			    'label'         => esc_html__('Slider Button Two Link','ewa-elementor-awareness'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);
		
		// Slider List
		$this->add_control(
			'ewa_slider_list',
			[
				'label' => esc_html__( 'Slider List', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ ewa_slider_title }}}',
			]
		);
		$this->end_controls_section();
		// End Controls Section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-awareness' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-awareness' ),
			]
		);

		// Slider Sub Title Options
		$this->add_control(
			'ewa_slider_sub_title_options',
			[
				'label' => esc_html__( 'Sub Title', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Slider Sub Title Color
		$this->add_control(
			'ewa_slider_sub_title_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .slider-block__content h3' => 'color: {{VALUE}}',
				],
			]
		);

		// Slider Sub Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_slider_sub_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .slider-block__content h3',
			]
		);

		// Slider Heading Options
		$this->add_control(
			'ewa_slider_heading_options',
			[
				'label' => esc_html__( 'Heading', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Slider Heading Color
		$this->add_control(
			'ewa_slider_heading_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .slider-block__content h2' => 'color: {{VALUE}}',
				],
			]
		);

		// Slider Heading Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_slider_heading_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .slider-block__content h2',
			]
		);		

		// Slider Button 1 Options
		$this->add_control(
			'ewa_slider_button1_options',
			[
				'label' => esc_html__( 'Button 1', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Slider Button 1 Color
		$this->add_control(
			'ewa_slider_button1_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .slider-block__button' => 'color: {{VALUE}}',
				],
			]
		);

		// Slider Button 1 Border Color
		$this->add_control(
			'ewa_slider_button1_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .slider-block__button' => 'border-color: {{VALUE}}',
				],
			]
		);

		// Slider Button 1 Background
		$this->add_control(
			'ewa_slider_button1_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .slider-block__button' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Slider Button 1 Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_slider_button1_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .slider-block__button',
			]
		);		

		// Slider Button 2 Options
		$this->add_control(
			'ewa_slider_button2_options',
			[
				'label' => esc_html__( 'Button 2', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Slider Button 2 Color
		$this->add_control(
			'ewa_slider_button2_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .slider-block__buttontwo' => 'color: {{VALUE}}',
				],
			]
		);

		// Slider Button 2 Border Color
		$this->add_control(
			'ewa_slider_button2_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .slider-block__buttontwo' => 'border-color: {{VALUE}}',
				],
			]
		);

		// Slider Button 2 Background
		$this->add_control(
			'ewa_slider_button2_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .slider-block__buttontwo' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Slider Button 2 Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_slider_button2_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .slider-block__buttontwo',
			]
		);

		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-awareness' ),
			]
		);	

		// Slider Button 1 Hover Options
		$this->add_control(
			'ewa_slider_button1_hover_options',
			[
				'label' => esc_html__( 'Button 1', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Slider Button 1 Hover Color
		$this->add_control(
			'ewa_slider_button1_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .slider-block__button:hover' => 'color: {{VALUE}}',
				],
			]
		);

		// Slider Button 1 Hover Border Color
		$this->add_control(
			'ewa_slider_button1_hover_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .slider-block__button:hover' => 'border-color: {{VALUE}}',
				],
			]
		);

		// Slider Button 1 Hover Background
		$this->add_control(
			'ewa_slider_button1_hover_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .slider-block__button:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Slider Button 1 Hover Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_slider_button1_hover_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .slider-block__button:hover',
			]
		);		

		// Slider Button 2 Hover Options
		$this->add_control(
			'ewa_slider_button2_hover_options',
			[
				'label' => esc_html__( 'Button 2', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Slider Button 2 Hover Color
		$this->add_control(
			'ewa_slider_button2_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .slider-block__buttontwo:hover' => 'color: {{VALUE}}',
				],
			]
		);

		// Slider Button 2 Hover Border Color
		$this->add_control(
			'ewa_slider_button2_hover_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .slider-block__buttontwo:hover' => 'border-color: {{VALUE}}',
				],
			]
		);

		// Slider Button 2 Hover Background
		$this->add_control(
			'ewa_slider_button2_hover_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .slider-block__buttontwo:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Slider Button 2 Hover Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_slider_button2_hover_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .slider-block__buttontwo:hover',
			]
		);

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section
	}

	/**
	 * Render slider widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */

	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		if ($settings['ewa_slider_list']){  ?>
        	<!-- Slider Start Here -->
            <div class="slider-block">
			    <?php 
				foreach ($settings['ewa_slider_list'] as $item){
					$slider_sub_title = $item['ewa_slider_sub_title'];
					$slider_title = $item['ewa_slider_title'];
					$slider_image = $item['ewa_slider_image'] ['url'];
					$slider_button_one_text = $item['ewa_slider_button_one_text'];
					$slider_button_one_link = $item['ewa_slider_button_one_link'] ['url'];
					$slider_button_two_text = $item['ewa_slider_button_two_text'];
					$slider_button_two_link = $item['ewa_slider_button_two_link'] ['url'];
				?>
                <div class="slider-block__item">
                    <div class= "slider-block__image" style="background-image: url('<?php echo esc_url($slider_image); ?>');">
			            <div class="slider-block__content text__center">
			                <h3><?php echo $slider_sub_title;?> </h3>
					        <h2><?php echo $slider_title;?></h2>
                            <a class="btn btn-outline slider-block__button" href="<?php echo esc_url($slider_button_one_link); ?>"> <?php echo $slider_button_one_text;?> </a>
                            <a class="btn btn-outline slider-block__buttontwo" href="<?php echo esc_url($slider_button_two_link); ?>"> <?php echo $slider_button_two_text;?>  </a>					
			            </div> <!-- end of slider-block__content -->
				    </div> <!-- end of slider-block__image -->
				</div> <!-- end of slider-block_item -->
				<?php } ?>
			</div> <!-- end of slider-block -->
		<!-- Slider End Here -->

		<?php }
	}
}