<?php
/**
 * EWA Elementor Brand Logo Widget.
 *
 * Elementor widget that inserts a brand logo into the page
 *
 * @since 1.0.0
 */
class EWA_Brand_Logo_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name. 
	 *
	 * Retrieve brand logo widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-brand-logo-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve brand logo widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Brand Logo', 'ewa-elementor-awareness' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve brand logo widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-band-aid';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the brand logo widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-elements' ];
	}

	/**
	 * Register brand logo widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
        
		//start of the Content tab section
		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'ewa-elementor-awareness' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);
		
		$repeater = new \Elementor\Repeater();
		
		// Brand Logo Title 
		$repeater->add_control(
			'ewa_brand_logo_title',
			[
				'label' => esc_html__( 'Title', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Company Title' , 'ewa-elementor-awareness' ),
			]
		);

		// Brand Logo Image
		$repeater->add_control(
			'ewa_brand_logo_image',
			[
				'label' => esc_html__( 'Choose Brand Logo Image', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'label_block'   => true,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		
		// Brand Logo List
		$this->add_control(
			'brand_logo_list',
			[
				'label' => esc_html__( 'Brand Logo List', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ ewa_brand_logo_title }}}',
			]
		);
		
		$this->end_controls_section();
		// End Controls Section
	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
        
		
		if ( $settings['brand_logo_list'] ) {  ?>

			<!-- Our Brand Start Here -->
			<div class="our-brand__slider">			
				<?php 
				foreach (  $settings['brand_logo_list'] as $item ) { 

					$brand_logo_title = $item['ewa_brand_logo_title'];
					$brand_logo_image = $item['ewa_brand_logo_image']['url'];
				?>
					<div class="brand-item">
						<img class="brand-img" src="<?php echo esc_url($brand_logo_image); ?>" />
					</div>
				<?php } ?>
			</div>

		<?php }
	}
}