<?php
/**
 * EWA Elementor Event Grid Widget.
 *
 * Elementor widget that inserts a porst grid into the page
 *
 * @since 1.0.0
 */
class EWA_Event_Grid_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve event grid widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-event-grid-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve post grid widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Event Grid', 'ewa-elementor-awareness' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve post grid widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-th';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the post grid widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-elements' ];
	}

	/**
	 * Register post grid widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		// start of Controls Section
		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'ewa-elementor-awareness' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);
		
		// Events Read More
        $this->add_control(
        	'ewa_events_readmore_text',
			[
				'label' => esc_html__( 'Button Text', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Load All Events', 'ewa-elementor-extension' ),
			]
		);
		
		// Events Button Link
		$this->add_control(
		    'ewa_events_readmore_link',
			[
			    'label'         => esc_html__('Button Link','ewa-elementor-awareness'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);

		$this->end_controls_section();
		// end of the source of the posts


		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-awareness' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-awareness' ),
			]
		);

		// Event Date Options
		$this->add_control(
			'ewa_event_date_options',
			[
				'label' => esc_html__( 'Date', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Event Date Color
		$this->add_control(
			'ewa_ewa_event_date_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .event-block__date' => 'color: {{VALUE}}',
				],
			]
		);

		// Event Date Background
		$this->add_control(
			'ewa_ewa_event_date_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .event-block__date' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Event Date Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_event_date_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .event-block__date',
			]
		);

		// Event Title Options
		$this->add_control(
			'ewa_event_title_options',
			[
				'label' => esc_html__( 'Title', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Event Title Color
		$this->add_control(
			'ewa_ewa_event_title_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .event-block__text h5 a' => 'color: {{VALUE}}',
				],
			]
		);

		// Event Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_event_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .event-block__text h5 a',
			]
		);

		// Event Description Options
		$this->add_control(
			'ewa_event_des_options',
			[
				'label' => esc_html__( 'Description', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Event Description Color
		$this->add_control(
			'ewa_ewa_event_des_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .event-block__des' => 'color: {{VALUE}}',
				],
			]
		);

		// Event Description Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_event_des_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .event-block__des',
			]
		);

		// Event Time Options
		$this->add_control(
			'ewa_event_time_options',
			[
				'label' => esc_html__( 'Time', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Event Time Color
		$this->add_control(
			'ewa_ewa_event_time_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .event-block__time' => 'color: {{VALUE}}',
				],
			]
		);

		// Event Time Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_event_time_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .event-block__time',
			]
		);

		// Event Location Options
		$this->add_control(
			'ewa_event_location_options',
			[
				'label' => esc_html__( 'Location', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Event Location Color
		$this->add_control(
			'ewa_ewa_event_location_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .event-block__location' => 'color: {{VALUE}}',
				],
			]
		);

		// Event Location Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_event_location_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .event-block__location',
			]
		);

		// Event Button Options
		$this->add_control(
			'ewa_event_btn_options',
			[
				'label' => esc_html__( 'Button', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Event Button Color
		$this->add_control(
			'ewa_event_btn_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .event-block__button' => 'color: {{VALUE}}',
				],
			]
		);
		// Event Button Border
		$this->add_control(
			'ewa_event_btn_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .event-block__button' => 'border-color: {{VALUE}}',
				],
			]
		);

		// Event Button Background
		$this->add_control(
			'ewa_event_btn_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .event-block__button' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Event Button Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_event_btn_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .event-block__button',
			]
		);

		// Event Options
		$this->add_control(
			'ewa_event_options',
			[
				'label' => esc_html__( 'Content Options', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Event Alignment
		$this->add_responsive_control(
			'ewa_event_alignment',
			[
				'label' => esc_html__( 'Alignment', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'ewa-elementor-awareness' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'ewa-elementor-awareness' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'ewa-elementor-awareness' ),
						'icon' => 'fa fa-align-right',
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'default' => 'left',
				'selectors' => [
					'{{WRAPPER}} .event-block__body' => 'text-align: {{VALUE}}',
				],
			]
		);
		
		// Event Border
		$this->add_control(
			'ewa_event_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#dfdfdf',
				'selectors' => [
					'{{WRAPPER}} .event-block__body' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-awareness' ),
			]
		);	

		// Event Button Hover Options
		$this->add_control(
			'ewa_event_btn_options',
			[
				'label' => esc_html__( 'Button', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Event Button Hover Color
		$this->add_control(
			'ewa_event_btn_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .event-block__button:hover' => 'color: {{VALUE}}',
				],
			]
		);
		// Event Button Hover Border
		$this->add_control(
			'ewa_event_btn_hover_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .event-block__button:hover' => 'border-color: {{VALUE}}',
				],
			]
		);

		// Event Button Hover Background
		$this->add_control(
			'ewa_event_btn_hover_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .event-block__button:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Event Button Hover Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_event_btn_hover_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .event-block__button:hover',
			]
		);

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section
		
	}

	/**
	 * Render post grid widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();		
		$events_readmore_text = $settings['ewa_events_readmore_text'];
		$events_readmore_link = $settings['ewa_events_readmore_link']['url'];

		$date_now = date('Y-m-d H:i:s');
		$query = new WP_Query( array(
			'post_type' => 'event',
			'posts_per_page' => 3,
			'meta_query' => array(
				array(
					'key'           => 'start_date',
					'compare'       => '>=',
					'value'         => $date_now,
					'type'          => 'DATE',
				)
			),
		) );
	
		if ( $query->have_posts() ) { ?>
			<div class="grid">
				<?php while ( $query->have_posts() ) : $query->the_post(); 			
					// get the featured image url
					$image_url = get_the_post_thumbnail_url( get_the_ID(), 'large' );
					$start_date = get_field('start_date');
					$start_time = get_field('start_time');
					$end_time = get_field('end_time');
					$venue_location = get_field('venue_location');
				?>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="event-block__body">
					<a href="<?php esc_url(the_permalink()); ?>" class="event-block__image" style="background-image:url('<?php echo esc_url($image_url); ?>')"></a>
						<div class="event-block__text">
							<span class="event-block__date"><?php echo $start_date;?></span>
							<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
							<p class="event-block__des"><?php the_content(); ?></p>

							<p class="event-block__time"><i class="far fa-clock"></i><?php echo $start_time;?> - <?php echo $end_time;?></p>
					
							<p class="event-block__location"><i class="fas fa-map-marker-alt"></i><?php echo $venue_location;?></p>
						</div>						
					</div> <!-- end of event-block__body -->
				</div> <!-- end of col xs -->
				<?php endwhile;
				wp_reset_postdata(); ?>
			</div>
			<div class="event-block__footer">
				<div class="grid">
					<div class="col-sm-12 col-md-12">
						<a href="<?php echo $events_readmore_link;?>" class="btn btn-outline event-block__button"><?php echo $events_readmore_text;?></a>
					</div>
				</div> <!-- end of grid -->

			</div>
		<?php 
		}
	}
}