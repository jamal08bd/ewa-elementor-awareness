<?php
/**
 * EWA Elementor Project Grid Widget.
 *
 * Elementor widget that inserts a porst grid into the page
 *
 * @since 1.0.0
 */
class EWA_Project_Grid_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve project grid widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-project-grid-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve post grid widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Project Grid', 'ewa-elementor-awareness' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve post grid widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-th';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the post grid widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-elements' ];
	}

	/**
	 * Register post grid widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		// start of Controls Section
		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'ewa-elementor-extension' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		// Source of the Project Grid
        $this->add_control(
        	'ewa_project_grid_from_categories_by_ids',
			[
				'label' => esc_html__( 'Project from Categories (Enter Category ids separated by comma)', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( '4,5', 'ewa-elementor-extension' ),
			]
		);

		$this->end_controls_section();
		// end of the source of the posts


		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-extension' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-extension' ),
			]
		);

		// Porject Raise Options
		$this->add_control(
			'ewa_project_raise_options',
			[
				'label' => esc_html__( 'Raise', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Project Raise Color
		$this->add_control(
			'ewa_project_raise_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .projects-block__raised' => 'color: {{VALUE}}',
				],
			]
		);

		// Porject Raise Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_project_raise_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .projects-block__raised',
			]
		);

		// Porject Goal Options
		$this->add_control(
			'ewa_project_goal_options',
			[
				'label' => esc_html__( 'Goal', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Project Goal Color
		$this->add_control(
			'ewa_project_goal_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .projects-block__goal' => 'color: {{VALUE}}',
				],
			]
		);

		// Porject Goal Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_project_goal_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .projects-block__goal',
			]
		);

		// Project Title Options
		$this->add_control(
			'ewa_project_title_options',
			[
				'label' => esc_html__( 'Title', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Project Title Color
		$this->add_control(
			'ewa_project_title_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .projects-block__details h5 a' => 'color: {{VALUE}}',
				],
			]
		);

		// Project Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_project_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .projects-block__details h5 a',
			]
		);

		// Project Description Options
		$this->add_control(
			'ewa_project_des_options',
			[
				'label' => esc_html__( 'Description', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Project Description Color
		$this->add_control(
			'ewa_project_des_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .projects-block__details p' => 'color: {{VALUE}}',
				],
			]
		);

		// project Description Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_project_des_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .projects-block__details p',
			]
		);

		// Project Button 1 Options
		$this->add_control(
			'ewa_project_button1_options',
			[
				'label' => esc_html__( 'Button 1', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Project Button 1 Color
		$this->add_control(
			'ewa_project_button1_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .projects-block__button' => 'color: {{VALUE}}',
				],
			]
		);

		// Project Button 1 Border Color
		$this->add_control(
			'ewa_project_button1_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .projects-block__button' => 'border-color: {{VALUE}}',
				],
			]
		);

		// Project Button 1 Background
		$this->add_control(
			'ewa_project_button1_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .projects-block__button' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Project Button 1 Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_project_button1_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .projects-block__button',
			]
		);

		//  Project Button 2 Options
		$this->add_control(
			'ewa_project_button2_options',
			[
				'label' => esc_html__( 'Button 2', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Project Button 2 Color
		$this->add_control(
			'ewa_project_button2_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .projects-block__buttontwo' => 'color: {{VALUE}}',
				],
			]
		);

		// Project Button 2 Border Color
		$this->add_control(
			'ewa_project_button2_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .projects-block__buttontwo' => 'border-color: {{VALUE}}',
				],
			]
		);

		// Project Button 2 Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_project_button2_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .projects-block__buttontwo',
			]
		);

		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-extension' ),
			]
		);	

		// Project Title Options
		$this->add_control(
			'ewa_project_title_hover_options',
			[
				'label' => esc_html__( 'Title', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Project Title Color
		$this->add_control(
			'ewa_project_title_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .projects-block__details h5 a:hover' => 'color: {{VALUE}}',
				],
			]
		);

		// Project Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_project_title_hover_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .projects-block__details h5 a:hover',
			]
		);
		
		//  Project Button 1 Options
		$this->add_control(
			'ewa_project_button1_hover_options',
			[
				'label' => esc_html__( 'Button 1', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Project Button 1 Color
		$this->add_control(
			'ewa_project_button1_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .projects-block__button:hover' => 'color: {{VALUE}}',
				],
			]
		);

		// Project Button 1 Hover Border Color
		$this->add_control(
			'ewa_project_button1_hover_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .projects-block__button:hover' => 'border-color: {{VALUE}}',
				],
			]
		);

		// Project Button 1 Hover Background
		$this->add_control(
			'ewa_project_button1_hover_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .projects-block__button:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Project Button 1 Hover Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_project_button1_hover_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .projects-block__button:hover',
			]
		);

		//  Project Button 2 Hover Options
		$this->add_control(
			'ewa_project_button2_hover_options',
			[
				'label' => esc_html__( 'Button 2', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Project Button 2 Color
		$this->add_control(
			'ewa_project_button2_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .projects-block__buttontwo:hover' => 'color: {{VALUE}}',
				],
			]
		);

		// Project Button 2 Hover Border Color
		$this->add_control(
			'ewa_project_button2_hover_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .projects-block__buttontwo:hover' => 'border-color: {{VALUE}}',
				],
			]
		);

		// Project Button 2 Hover Background
		$this->add_control(
			'ewa_project_button2_hover_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .projects-block__buttontwo:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Project Button 2 Hover Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_project_button2_hover_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .projects-block__buttontwo:hover',
			]
		);

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render post grid widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
			$settings = $this->get_settings_for_display();
			$project_btn1_text = $settings['ewa_project_btn1_text'];
			$project_btn2_text = $settings['ewa_project_btn2_text'];

			$args = array(
			'posts_per_page' => 2,
			'post_type' => 'project',
			'post_status' => 'publish',
			);
			$query = new WP_Query($args);
       ?>

       	<!-- Project grid Start Here -->
			<div class="grid">

				<?php 
					if ($query->have_posts()) :

	          		while ($query->have_posts()) : $query->the_post(); 

	          		// get the featured image url
					$image_url = get_the_post_thumbnail_url( get_the_ID(), 'large' );
					$raised = get_field('raised');
					$goal = get_field('goal');
					$donate_text = get_field('donate_text');
					$donate_link = get_field('donate_link');
					$view_text = get_field('view_text');
				?>
				<div class="col-md-6 col-sm-12">
					<div class="projects-block__body">
						<a href="<?php esc_url(the_permalink()); ?>" class="projects-block__image" style="background-image:url('<?php echo esc_url($image_url); ?>')"></a>
				
						<div class="projects-block__text">
							<div class="projects-block__value">
								<span class="projects-block__raised">Raised: $<?php echo number_format($raised, 0, ',', ',');?> /</span>
								<span class="projects-block__goal">Goal: $<?php echo number_format($goal, 0, ',', ',');?></span>
							</div> 
						
							<div class="projects-block__details">
								<h5> <a href="<?php esc_url(the_permalink()); ?>"><?php the_title();?></a></h5>
								<?php the_content();?>
							</div> 
						
							<div class="projects-block__footer">
								<a class="btn btn-outline projects-block__button" href="<?php echo esc_url($donate_link);?>" target="_blank"><?php echo $donate_text;?></a>
								<a class="btn btn-outline projects-block__buttontwo" href="<?php esc_url(the_permalink()); ?>"><?php echo $view_text;?></a>
							</div>
						</div> <!-- end of projects-block__text -->		
					</div> <!-- end of projects-block__body  -->
				</div>
				<?php
			endwhile;

		endif; 
		?>

		</div> <!-- end of grid -->
		<!-- Project grid End Here -->

       <?php
	}
}