<?php
/**
 * EWA Elementor Team 2 Widget.
 *
 * Elementor widget that inserts team into the page
 *
 * @since 1.0.0
 */
class EWA_Team_2_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve team widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-team-2-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve team widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Team 2', 'ewa-elementor-awareness' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve team widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-user-friends';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the team widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-elements' ];
	}

	/**
	 * Register team widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

        // start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-awareness'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// Team two Image
		$this->add_control(
		    'ewa_team_two_image',
			[
			    'label' => esc_html__('Choose Team Image','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
				    'url' => \Elementor\Utils::get_placeholder_image_src(),          
				],
			]
		);
		
		// Team Two Name
		$this->add_control(
		    'ewa_team_two_name',
			[
			    'label' => esc_html__('Name','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__('Enter Team Name','ewa-elementor-awareness'),
			]
		);
		
		//Team Two Title
		$this->add_control(
		    'ewa_team_two_title',
			[
			    'label' => esc_html__('Title','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__('Enter Team Title','ewa-elementor-awareness'),
			]
		);
		
		// Team Two Facebook Link
        $this->add_control(
        	'ewa_team_two_facebook_link',
			[
				'label'         => esc_html__('Facebook Link', 'ewa-elementor-awareness'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
        );
		
		// Team Two Twitter Link
        $this->add_control(
        	'ewa_team_two_twitter_link',
			[
				'label'         => esc_html__('Twitter Link', 'ewa-elementor-awareness'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
        );
		
		// Team Two Gmail Link
        $this->add_control(
        	'ewa_team_two_gmail_link',
			[
				'label'         => esc_html__('Gmail Link', 'ewa-elementor-awareness'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
        );
		
		// Team Two Linkedin Link
        $this->add_control(
        	'ewa_team_two_linkedin_link',
			[
				'label'         => esc_html__('Linkedin Link', 'ewa-elementor-awareness'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
        );
		
		$this->end_controls_section();
		// end of the Content tab section

		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-awareness' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-awareness' ),
			]
		);

		// Team Two Name Options
		$this->add_control(
			'ewa_team_two_name_options',
			[
				'label' => esc_html__( 'Name', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Team Two Name Color
		$this->add_control(
			'ewa_team_two_name_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .team-two__member h3' => 'color: {{VALUE}}',
				],
			]
		);

		// Team Two Name Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_team_two_name_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .team-two__member h3',
			]
		);

		// Team Two Title Options
		$this->add_control(
			'ewa_team_two_title_options',
			[
				'label' => esc_html__( 'Title', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Team Two Title Color
		$this->add_control(
			'ewa_team_two_title_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .team-two__member h4' => 'color: {{VALUE}}',
				],
			]
		);

		// Team Two Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_team_two_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .team-two__member h4',
			]
		);

		// Team Two Social Options
		$this->add_control(
			'ewa_team_two_social_options',
			[
				'label' => esc_html__( 'Social Link', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Team Two Social Color
		$this->add_control(
			'ewa_team_two_social_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .team-two__socialprofile a' => 'color: {{VALUE}}',
				],
			]
		);

		// Team Two Social Background
		$this->add_control(
			'ewa_team_two_social_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .team-two__socialprofile a' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Team Two Social Border
		$this->add_control(
			'ewa_team_two_social_border',
			[
				'label' => esc_html__( 'Border', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .team-two__socialprofile a' => 'border-color: {{VALUE}}',
				],
			]
		);

		// Team Two Social Separator Color
		$this->add_control(
			'ewa_team_two_social_separator_color',
			[
				'label' => esc_html__( 'Separator Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#dfdfdf',
				'selectors' => [
					'{{WRAPPER}} .team-two__socialprofile' => 'border-top: 1px solid {{VALUE}}',
				],
			]
		);

		// Team Two Content Options
		$this->add_control(
			'ewa_team_two_content_options',
			[
				'label' => esc_html__( 'Content Options', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Team Two Content Background
		$this->add_control(
			'ewa_team_two_content_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#F9FAFB',
				'selectors' => [
					'{{WRAPPER}} .team-two__member' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-awareness' ),
			]
		);	
		
		// Team Two Social Hover Options
		$this->add_control(
			'ewa_team_two_social_hover_options',
			[
				'label' => esc_html__( 'Social Link Hover', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Team Two Social Hover Color
		$this->add_control(
			'ewa_team_social_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .team-two__socialprofile a:hover' => 'color: {{VALUE}}',
				],
			]
		);

		// Team Two Social Hover Background
		$this->add_control(
			'ewa_team_social_hover_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .team-two__socialprofile a:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Team Two Social Hover Border
		$this->add_control(
			'ewa_team_social_hover_border',
			[
				'label' => esc_html__( 'Border', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .team-two__socialprofile a:hover' => 'border-color: {{VALUE}}',
				],
			]
		);	

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section
	}


	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$team_two_image = $settings['ewa_team_two_image']['url'];
		$team_two_name = $settings['ewa_team_two_name'];
		$team_two_title = $settings['ewa_team_two_title'];
		$team_two_facebook_link = $settings['ewa_team_two_facebook_link'];
		$team_two_twitter_link = $settings['ewa_team_two_twitter_link'];
		$team_two_gmail_link = $settings['ewa_team_two_gmail_link'];
		$team_two_linkedin_link = $settings['ewa_team_two_linkedin_link'];

       ?>

       	<!-- Team 2 Start Here -->
			<div class="team-two__member text__center">
				<figure class="team-two__info">
				    <a href="#" title="<?php echo $team_two_name; ?>">
                    <img src="<?php echo $team_two_image; ?>" />
                    </a>
				</figure> <!-- end of ourteam-two__info -->
			    <h3><?php echo $team_two_name; ?></h3>                                   
                <h4><?php echo $team_two_title; ?></h4>                             
                <div class="team-two__socialprofile">
                    <a href="<?php echo esc_url($team_two_facebook_link); ?>" title="Facebook"><i class="fab fa-facebook-f"></i></a>
                    <a href="<?php echo esc_url($team_two_twitter_link); ?>" title="Twitter"><i class="fab fa-twitter"></i></a>
                    <a href="<?php echo esc_url($team_two_gmail_link); ?>" title="Google Plus"><i class="fab fa-google-plus-g"></i></a>
					<a href="<?php echo esc_url($team_two_linkedin_link); ?>" title="Linkedin"><i class="fab fa-linkedin-in"></i></a>
                </div> <!-- end of ourteam-two__socialprofile -->
			</div> <!-- end of ourteam-two__member -->
		<!-- Team 2 End Here -->

       <?php
	}
}