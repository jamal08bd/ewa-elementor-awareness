<?php
/**
 * EWA Elementor Hero Widget.
 *
 * Elementor widget that inserts hero into the page
 *
 * @since 1.0.0
 */
class EWA_Hero_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Hero widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-hero-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve hero widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Hero', 'ewa-elementor-awareness' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve hero widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-sliders-h';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the hero widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-elements' ];
	}

	/**
	 * Register hero widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

       // start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-awareness'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// Hero Image
		$this->add_control(
		    'ewa_hero_image',
			[
			    'label' => esc_html__('Choose Hero Image','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
				    'url' => \Elementor\Utils::get_placeholder_image_src(),          
				],
			]
		);
		
		//Hero Title
		$this->add_control(
		    'ewa_hero_title',
			[
			    'label' => esc_html__('Title','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__('Enter Hero Title','ewa-elementor-awareness'),
			]
		);
		
		//Hero Heading
		$this->add_control(
		    'ewa_hero_heading',
			[
			    'label' => esc_html__('Heading','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__('Enter Hero Heading','ewa-elementor-awareness'),
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section

		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-awareness' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-awareness' ),
			]
		);

		// Hero Title Options
		$this->add_control(
			'ewa_hero_title_options',
			[
				'label' => esc_html__( 'Title', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Hero Title Color
		$this->add_control(
			'ewa_hero_title_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .hero-block__title span' => 'color: {{VALUE}}',
				],
			]
		);

		// Hero Title Background Color
		$this->add_control(
			'ewa_hero_title_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => 'rgba(119, 199, 32, 0.7)',
				'selectors' => [
					'{{WRAPPER}} .hero-block__title span' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Hero Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_hero_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .hero-block__title span',
			]
		);

		// Hero Heading Options
		$this->add_control(
			'ewa_hero_heading_options',
			[
				'label' => esc_html__( 'Heading', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Hero Heading Color
		$this->add_control(
			'ewa_hero_heading_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .hero-block__heading' => 'color: {{VALUE}}',
				],
			]
		);

		// Hero Heading Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_hero_heading_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .hero-block__heading',
			]
		);

		// Team Alignment Options
		$this->add_control(
			'ewa_team_alignment_options',
			[
				'label' => esc_html__( 'Content Alignment', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Hero Alignment
		$this->add_responsive_control(
			'ewa_hero_alignment',
			[
				'label' => esc_html__( 'Alignment', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'ewa-elementor-awareness' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'ewa-elementor-awareness' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'ewa-elementor-awareness' ),
						'icon' => 'fa fa-align-right',
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'default' => 'center',
				'selectors' => [
					'{{WRAPPER}} .hero-block' => 'text-align: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-awareness' ),
			]
		);	

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section
	}

	/**
	 * Render hero widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */

	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$hero_image = $settings['ewa_hero_image']['url'];
		$hero_title = $settings['ewa_hero_title'];
		$hero_heading = $settings['ewa_hero_heading'];

       ?>

       	<!-- Hero Start Here -->
           
			<div class="hero-block text__center" style="background-image: url('<?php echo $hero_image; ?>');">
			    <div class="col-md-12 col-sm-12">
			        <h3 class="hero-block__title"><span><?php echo $hero_title;  ?></span></h3>
					<h2 class="hero-block__heading"><?php echo $hero_heading;  ?></h2>
			    </div> <!-- end of sm -->
			</div>
			
		<!-- Hero End Here -->

       <?php
	}
}