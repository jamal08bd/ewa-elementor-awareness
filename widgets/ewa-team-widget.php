<?php
/**
 * EWA Elementor Team Widget.
 *
 * Elementor widget that inserts team into the page
 *
 * @since 1.0.0
 */
class EWA_Team_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve team widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-team-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve team widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Team', 'ewa-elementor-awareness' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve team widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-user-friends';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the team widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-elements' ];
	}

	/**
	 * Register team widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

       // start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-awareness'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// Team Image
		$this->add_control(
		    'ewa_team_image',
			[
			    'label' => esc_html__('Choose Team Image','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
				    'url' => \Elementor\Utils::get_placeholder_image_src(),          
				],
			]
		);
		
		// Team Name
		$this->add_control(
		    'ewa_team_name',
			[
			    'label' => esc_html__('Name','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__('Enter Team Name','ewa-elementor-awareness'),
			]
		);
		
		//Team Title
		$this->add_control(
		    'ewa_team_title',
			[
			    'label' => esc_html__('Title','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__('Enter Team Title','ewa-elementor-awareness'),
			]
		);
		
		//Team Description
		$this->add_control(
		    'ewa_team_des',
			[
			    'label' => esc_html__('Team Description','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__('Enter Team Description','ewa-elementor-awareness'),
			]
		);
		
		// Team Facebook Link
        $this->add_control(
        	'ewa_team_facebook_link',
			[
				'label'         => esc_html__('Facebook Link', 'ewa-elementor-awareness'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
        );
		
		// Team Twitter Link
        $this->add_control(
        	'ewa_team_twitter_link',
			[
				'label'         => esc_html__('Twitter Link', 'ewa-elementor-awareness'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
        );
		
		// Team Linkedin Link
        $this->add_control(
        	'ewa_team_linkedin_link',
			[
				'label'         => esc_html__('Linkedin Link', 'ewa-elementor-awareness'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
        );
		
		$this->end_controls_section();
		// end of the Content tab section

		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-awareness' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-awareness' ),
			]
		);

		// Team Name Options
		$this->add_control(
			'ewa_team_name_options',
			[
				'label' => esc_html__( 'Name', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Team Name Color
		$this->add_control(
			'ewa_team_name_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .team-one__member h3' => 'color: {{VALUE}}',
				],
			]
		);

		// Team Name Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_team_name_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .team-one__member h3',
			]
		);

		// Team Title Options
		$this->add_control(
			'ewa_team_titl_options',
			[
				'label' => esc_html__( 'Title', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Team Title Color
		$this->add_control(
			'ewa_team_title_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .team-one__member h4' => 'color: {{VALUE}}',
				],
			]
		);

		// Team Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_team_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .team-one__member h4',
			]
		);

		// Team Description Options
		$this->add_control(
			'ewa_team_des_options',
			[
				'label' => esc_html__( 'Description', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Team Description Color
		$this->add_control(
			'ewa_team_des_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .team-one__member p' => 'color: {{VALUE}}',
				],
			]
		);

		// Team Description Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_team_des_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .team-one__member p',
			]
		);

		// Team Social Options
		$this->add_control(
			'ewa_team_social_options',
			[
				'label' => esc_html__( 'Social Link', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Team Social Color
		$this->add_control(
			'ewa_team_social_color',
			[
				'label' => esc_html__( 'Text Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .team-one__socialprofile a' => 'color: {{VALUE}}',
				],
			]
		);

		// Team Social Separator Color
		$this->add_control(
			'ewa_team_social_separator_color',
			[
				'label' => esc_html__( 'Separator Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .team-one__socialprofile a:after' => 'color: {{VALUE}}',
				],
			]
		);

		// Team Social Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_team_social_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .team-one__socialprofile a',
			]
		);		

		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-awareness' ),
			]
		);	
		
		// Team Social Hover Options
		$this->add_control(
			'ewa_team_social_hover_options',
			[
				'label' => esc_html__( 'Social Link Hover', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Team Social Hover Color
		$this->add_control(
			'ewa_team_social_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .team-one__socialprofile a:hover' => 'color: {{VALUE}}',
				],
			]
		);

		// Team Social Hover Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_team_social_hover_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .team-one__socialprofile a:hover',
			]
		);	

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section
	}


	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$team_image = $settings['ewa_team_image']['url'];
		$team_name = $settings['ewa_team_name'];
		$team_title = $settings['ewa_team_title'];
		$team_description = $settings['ewa_team_des'];
		$team_facebook_link = $settings['ewa_team_facebook_link'];
		$team_twitter_link = $settings['ewa_team_twitter_link'];
		$team_linkedin_link = $settings['ewa_team_linkedin_link'];
		

       ?>

       	<!-- Team Start Here -->
			<div class= "team-one__member">
			    <figure class="team-one__info">
				    <a href="#" title="<?php echo $team_name; ?>">
                    <img src="<?php echo $team_image; ?>" />
                    </a>
				</figure>
				<h3><?php echo $team_name; ?></h3>                                   
                <h4><?php echo $team_title; ?></h4>                             
                <p><?php echo $team_description; ?></p>
				<div class="team-one__socialprofile">
                    <a href="<?php echo esc_url($team_facebook_link); ?>" title="Facebook">Facebook</a>
                    <a href="<?php echo esc_url($team_twitter_link); ?>" title="Twitter">Twitter</a>
                    <a href="<?php echo esc_url($team_linkedin_link); ?>" title="Linkedin">Linkedin</a>
                </div> <!-- end social-profile  -->
			</div> <!-- end of team one -->
		<!-- Team End Here -->

       <?php
	}
}