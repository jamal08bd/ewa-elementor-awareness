<?php
/**
 * EWA Elementor Post Grid Widget.
 *
 * Elementor widget that inserts a porst grid into the page
 *
 * @since 1.0.0
 */
class EWA_Post_Grid_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve post grid widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-post-grid-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve post grid widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Post Grid', 'ewa-elementor-awareness' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve post grid widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-th';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the post grid widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-elements' ];
	}

	/**
	 * Register post grid widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		// start of Controls Section
		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'ewa-elementor-extension' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		// Source of the posts
        $this->add_control(
        	'ewa_posts_from_categories_by_ids',
			[
				'label' => esc_html__( 'Post from Categories (Enter Category ids separated by comma)', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( '4,5', 'ewa-elementor-extension' ),
			]
        );

		$this->end_controls_section();
		// end of the source of the posts


		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-extension' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-extension' ),
			]
		);

		// Post One Title Options
		$this->add_control(
			'ewa_post_one_title_options',
			[
				'label' => esc_html__( 'Title', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Post One Tite Color
		$this->add_control(
			'ewa_post_one_title_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .postgrid-one__title a' => 'color: {{VALUE}}',
				],
			]
		);

		// Post One Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_post_one_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .postgrid-one__title a',
			]
		);

		// Post One Description Options
		$this->add_control(
			'ewa_post_one_des_options',
			[
				'label' => esc_html__( 'Description', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Post One Description Color
		$this->add_control(
			'ewa_post_one_des_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .postgrid-one p' => 'color: {{VALUE}}',
				],
			]
		);

		// Post One Description Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_post_one_des_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .postgrid-one p',
			]
		);

		// Post One Link Options
		$this->add_control(
			'ewa_post_one_link_options',
			[
				'label' => esc_html__( 'Link', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Post One Link Color
		$this->add_control(
			'ewa_post_one_link_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .postgrid-one__author, .postgrid-one__comments' => 'color: {{VALUE}}',
				],
			]
		);

		// Post One Link Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_post_one_link_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .postgrid-one__author, .postgrid-one__comments',
			]
		);

		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-extension' ),
			]
		);	
		
		// Post One Title Hover Options
		$this->add_control(
			'ewa_post_one_title_hover_options',
			[
				'label' => esc_html__( 'Title', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Post One Title Hover Color
		$this->add_control(
			'ewa_post_one_title_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-extension' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .postgrid-one__title a:hover' => 'color: {{VALUE}}',
				],
			]
		);

		// Post One Title Hover Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_post_one_title_hover_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .postgrid-one__title a:hover',
			]
		);	

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section
	}

	/**
	 * Render post grid widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		// post from categories getting category ID's
		$cat_ids = $settings['ewa_posts_from_categories_by_ids'];		

		$query = new WP_Query( array(
			'post_type' => 'post',
			'posts_per_page' => 3,
			'cat' => $cat_ids,
		) );
	
		if ( $query->have_posts() ) { ?>
			<div class="grid post-query">
				<?php while ( $query->have_posts() ) : $query->the_post(); 
				
					// get the featured image url
					$image_url = get_the_post_thumbnail_url( get_the_ID(), 'large' );
					// get the post date
					$post_date = get_the_date('M j, Y');

					// get the category name and link
					$term_list = wp_get_post_terms(get_the_ID(), 'category', ['fields' => 'all']);

					$cat_name = '';
				   $cat_link = '';

				   foreach ($term_list as $term) {
					if (get_post_meta(get_the_ID(), '_yoast_wpseo_primary_category', true) == $term->term_id) {
					  $cat_name = $term->name;
					  $cat_link = get_term_link($term->term_id);
					 break;
					}
					else {
					  $cat_name = $term->name;
					  $cat_link = get_term_link($term->term_id);
					}
				  }
				?>
			<div class="col-md-6 col-sm-12">
				<div class="postgrid-one">
					<a href="<?php esc_url(the_permalink());?>" class="postgrid-one__image" style="background-image:url('<?php echo esc_url($image_url); ?>')"></a>
					<h4 class="postgrid-one__title">
					<a href="<?php esc_url(the_permalink()); ?>"><?php the_title(); ?></a>
					</h4>
					<?php the_excerpt();?>
					<div class="postgrid-one__details">
						<a class="postgrid-one__author" href="#" ><i class="far fa-edit"></i><?php the_author_meta('display_name'); ?></a>
						<a class="postgrid-one__comments" href="#" ><i class="far fa-comments"></i> <?php echo get_comments_number();?> comments</a>
					</div>
				</div>
			</div>
			<?php endwhile;
			wp_reset_postdata(); ?>
		</div>
		<?php 
		}
	}
}