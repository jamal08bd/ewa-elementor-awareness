<?php
/**
 * EWA Elementor CTA Widget.
 *
 * Elementor widget that inserts a call to action into the page
 *
 * @since 1.0.0
 */
class EWA_CTA_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve call to action widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-cta-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve oEmbed widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA CTA', 'ewa-elementor-awareness' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve oEmbed widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-newspaper';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the call to action widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-elements' ];
	}

	/**
	 * Register oEmbed widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		// start of the Content tab section
		$this->start_controls_section(
			'content-section',
			[
				'label' => esc_html__('Content','ewa-elementor-awareness'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
			
			]
		);
			 
		// CTA Title
		$this->add_control(
			'ewa_cta_title',
			[
					'label' => esc_html__('CTA Title','ewa-elementor-awareness'),
					'type' => \Elementor\Controls_Manager::TEXT,
					'label_block' => true,
					'default' => esc_html__('Our Achievements', 'ewa-elementor-awareness')
			]
		);
			
		// CTA Description
		$this->add_control(
			'ewa_cta_des',
			[
				'label' => esc_html__('CTA Description','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__('We are researching and developing many projects to fight against the global warming And ensure a better life.','ewa-elementor-awareness'),
			]
		);

		// CTA Button Text
		$this->add_control(
			'ewa_cta_btn_text',
			[
				'label' => esc_html__('CTA Button Text','ewa-elementor-awareness'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__('Get Started', 'ewa-elementor-awareness')
			]
		);

		// CTA Button Link
		$this->add_control(
		    'ewa_cta_btn_link',
			[
			    'label' => esc_html__('CTA Button Link','ewa-elementor-awareness'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);
			 
		$this->end_controls_section();
		// end of the Content tab section

		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-awareness' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-awareness' ),
			]
		);

		// CTA Title Options
		$this->add_control(
			'ewa_cta_title_options',
			[
				'label' => esc_html__( 'CTA Title', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// CTA Title Color
		$this->add_control(
			'ewa_cta_title_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .cta__title' => 'color: {{VALUE}}',
				],
			]
		);

		// CTA Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_cta_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .cta__title',
			]
		);

		// CTA Description Options
		$this->add_control(
			'ewa_cta_des_options',
			[
				'label' => esc_html__( 'CTA Description', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// CTA Description Color
		$this->add_control(
			'ewa_cta_des_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .cta__des' => 'color: {{VALUE}}',
				],
			]
		);

		// CTA Description Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_cta_des_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .cta__des',
			]
		);

		// CTA Button Options
		$this->add_control(
			'ewa_cta_btn_options',
			[
				'label' => esc_html__( 'CTA Button', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// CTA Button Color
		$this->add_control(
			'ewa_cta_btn_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .cta__btn' => 'color: {{VALUE}}',
				],
			]
		);

		// CTA Button Background
		$this->add_control(
			'ewa_cta_btn_back',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .cta__btn' => 'background-color: {{VALUE}}',
				],
			]
		);

		// CTA Button Border
		$this->add_control(
			'ewa_cta_btn_border',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .cta__btn' => 'border-color: {{VALUE}}',
				],
			]
		);

		// CTA Button Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_cta_btn_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .cta__btn',
			]
		);

		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-awareness' ),
			]
		);	

		// CTA Button Hover Options
		$this->add_control(
			'ewa_cta_btn_hover_options',
			[
				'label' => esc_html__( 'CTA Button Hover', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// CTA Button Hover Color
		$this->add_control(
			'ewa_cta_btn_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .cta__btn:hover' => 'color: {{VALUE}}',
				],
			]
		);

		// CTA Button Hover Background
		$this->add_control(
			'ewa_cta_btn_hover_back',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .cta__btn:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		// CTA Button Hover Border
		$this->add_control(
			'ewa_cta_btn_hover_border',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-awareness' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#77C720',
				'selectors' => [
					'{{WRAPPER}} .cta__btn:hover' => 'border-color: {{VALUE}}',
				],
			]
		);

		// CTA Button Hover Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_cta_btn_hover_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-awareness' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .cta__btn:hover',
			]
		);

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render call to action widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();

		$cta_title = $settings['ewa_cta_title'];
		$cta_des = $settings['ewa_cta_des'];
		$cta_btn_text = $settings['ewa_cta_btn_text'];
		$cta_btn_link = $settings['ewa_cta_btn_link']['url'];
       ?>

       	<!-- CTA Start Here -->
			<div class="cta">
				<h2 class="cta__title"><?php echo $cta_title;?></h2>
				<p class="cta__des"><?php echo $cta_des;?></p>
				<a href="<?php echo esc_url($cta_btn_link);?>" class="cta__btn"><?php echo $cta_btn_text;?></a>
			</div>
		<!-- CTA End Here -->

       <?php
	}
}