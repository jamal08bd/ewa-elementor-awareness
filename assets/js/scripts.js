( function( $ ) {

// put all theme related jQuery here

		/*
		*  slick slide for main Hero Slider start here
		* docs from http://kenwheeler.github.io/slick/ and https://github.com/kenwheeler/slick
		*/
		$('.slider-block').slick({
		  dots: false,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  autoplay: false,
		  autoplaySpeed: 4000,
		  arrows: true,
		  fade: true,
		    responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
		});
		/* slick slide for main Hero Slider end here */


		/*
		*  slick slide for Brand start here
		* docs from http://kenwheeler.github.io/slick/ and https://github.com/kenwheeler/slick
		*/
		$('.our-brand__slider').slick({
		  dots: true,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 6,
		  slidesToScroll: 1,
		  autoplay: true,
		  autoplaySpeed: 4000,
		  arrows: false,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: true
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
		});
		/* slick slide for Brand end here */


		$('.post-query').slick({
			dots: true,
			infinite: true,
			speed: 300,
			slidesToShow: 2,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 4000,
			arrows: true,
			responsive: [
			  {
				breakpoint: 1024,
				settings: {
				  slidesToShow: 3,
				  slidesToScroll: 1,
				  infinite: true,
				  dots: true
				}
			  },
			  {
				breakpoint: 600,
				settings: {
				  slidesToShow: 2,
				  slidesToScroll: 1
				}
			  },
			  {
				breakpoint: 480,
				settings: {
				  slidesToShow: 1,
				  slidesToScroll: 1
				}
			  }
			  // You can unslick at a given breakpoint now by adding:
			  // settings: "unslick"
			  // instead of a settings object
			]
		});

		  /* Counter start here */
		$('.counter').counterUp({
			delay: 10,
			time: 2000
		});
		/* Counter end here */
		

 } )( jQuery );
